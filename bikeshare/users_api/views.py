# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.generics import CreateAPIView,ListCreateAPIView, ListAPIView, RetrieveAPIView
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import render
from django.contrib.auth import get_user_model
from rest_framework.status import HTTP_200_OK,HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser
from .models import Profile, Subscriptions, Account, Membership
from .c2b import sendSTK
import json


from .serializers import AccountCreateSerializer,ProfileCreateSerializer,UserLoginSerializer,AllUsersSerializer,SubscriptionSerializer, MembershipCreateSerializer, CheckNumberSerializer


User = get_user_model

# Create your views here.
class UserListAPIView(ListCreateAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = AccountCreateSerializer
    queryset = Account.objects.all()

class UserCreateAPIView(CreateAPIView):
    """ create user"""
    permission_classes = [AllowAny,]
    serializer_class = AccountCreateSerializer
    #queryset = User.objects.all()

class UserByUsernameView(RetrieveAPIView):
    permission_classes = [AllowAny, ]
    serializer_class = AccountCreateSerializer
    queryset = Account.objects.all()
    lookup_field = 'username'


class ProfileCreateAPIView(CreateAPIView):
    """ Create profile """
    permission_classes = [AllowAny,]
    serializer_class = ProfileCreateSerializer
    #queryset = Profile.objects.all()

class UserLoginAPIView(APIView):
    """ User login view """
    permission_classes = [AllowAny,]
    serializer_class = UserLoginSerializer
    

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = UserLoginSerializer(data = data)
        if serializer.is_valid(raise_exception=True):
            new_data = serializer.data
            return Response(new_data, status = HTTP_200_OK)
        return Response(serializer.errors, status = HTTP_400_BAD_REQUEST)

class GetUsersView(APIView):
    queryset = Account.objects.all()

class CheckNumberView(APIView):
    permission_classes = [AllowAny,]
    queryset = Account.objects.all()
    serializer_class = CheckNumberSerializer

    def post(self, request, *args, **kwargs):
        data = request.data
        phone_number = data['phone_number']
        account = None
        new_data = {}
        account = Account.objects.filter(phone_number = phone_number)
        if account :
            new_data['status'] = "Successful"
            return Response(new_data, status=HTTP_200_OK)
        new_data['status'] = "Not Found"
        return Response(new_data, status=HTTP_200_OK)


#Subscriptions views
class SubscriptionCreateView(ListCreateAPIView):
    """ Create behaviour of subscriptions"""
    permission_classes = [AllowAny,]
    queryset = Subscriptions.objects.all()
    serializer_class = SubscriptionSerializer

    def perform_create(self, serializer):
        """Save the post data when creating a new subscription."""
        serializer.save()

#Membership views
class MembershipCreateView(ListCreateAPIView):
    """ Membership create view """
    permission_classes = [AllowAny,]
    queryset = Membership.objects.all()
    serializer_class = MembershipCreateSerializer

    def perform_create(self, serializer):
        """Save the post data when creating a new membership."""
        serializer.save()


class SubmitView(APIView):
    permission_classes = [AllowAny,]

    def post(self, request):
        data = request.data
        phone_number = data['phone_number']
        amount = data['amount']

        print(phone_number)
        print(amount)
        sendSTK(phone_number, amount)
        # b2c()
        message = {"status": "ok"}
        return Response(message, status=HTTP_200_OK)


class ConfirmView(APIView):
    permission_classes = [AllowAny,]

    def post(self, request):
        # save the data
        request_data = json.dumps(request.data)
        request_data = json.loads(request_data)
        body = request_data.get('Body')
        resultcode = body.get('stkCallback').get('ResultCode')
        # Perform your processing here e.g. print it out...
        if resultcode == 0:
            print('Payment successful')
        else:
            print ('unsuccessfull')

        # Prepare the response, assuming no errors have occurred. Any response
        # other than a 0 (zero) for the 'ResultCode' during Validation only means
        # an error occurred and the transaction is cancelled
        message = {
            "ResultCode": 0,
            "ResultDesc": "The service was accepted successfully",
            "ThirdPartyTransID": "1237867865"
        }

        # Send the response back to the server
        return Response(message, status=HTTP_200_OK)

    def get(self, request):
        return Response("Confirm callback", status=HTTP_200_OK)


class ValidateView(APIView):
    permission_classes = [AllowAny,]

    def post(self, request):
        # save the data
        request_data = request.data

        # Perform your processing here e.g. print it out...
        print("validate data" + request_data)

        # Prepare the response, assuming no errors have occurred. Any response
        # other than a 0 (zero) for the 'ResultCode' during Validation only means
        # an error occurred and the transaction is cancelled
        message = {
            "ResultCode": 0,
            "ResultDesc": "The service was accepted successfully",
            "ThirdPartyTransID": "1234567890"
        };

        # Send the response back to the server
        return Response(message, status=HTTP_200_OK)
