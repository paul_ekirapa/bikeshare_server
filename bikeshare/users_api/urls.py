"""bikeshare URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from .views import UserCreateAPIView, ProfileCreateAPIView, UserLoginAPIView, SubscriptionCreateView, \
    MembershipCreateView, UserListAPIView, CheckNumberView, UserByUsernameView, ValidateView, ConfirmView, SubmitView

urlpatterns = [
    url(r'^users/', UserListAPIView.as_view(), name='register'),
    url(r'^user/checknumber/', CheckNumberView.as_view(), name='check-number'),
    url(r'^user/register/', UserCreateAPIView.as_view(), name='register'),
    url(r'^user/(?P<username>\w+)/$', UserByUsernameView.as_view(), name='user-view-username'),
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^api-token-refresh/', refresh_jwt_token),
    url(r'^user/login/', UserLoginAPIView.as_view(), name='login'),
    url(r'^user/create/profile/', ProfileCreateAPIView.as_view(), name='create-profile'),
    url(r'^create/subscription/', SubscriptionCreateView.as_view(), name='create-subscription'),
    url(r'^create/membership/', MembershipCreateView.as_view(), name='create-membership'),
    url(r'^validate/', ValidateView.as_view(), name='validate'),
    url(r'^confirm/', ConfirmView.as_view(), name='confirm'),
    url(r'^submit/', SubmitView.as_view(), name='submit'),
]
