from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.serializers import ValidationError,CharField,EmailField
from .models import Account,Profile,Subscriptions,Membership
from django.db.models import Q

User = get_user_model()


class AccountCreateSerializer(serializers.ModelSerializer):    
    class Meta:
        """ Account serializer """
        model = Account
        fields = ['id', 'user_uuid', 'username','phone_number','email', 'password']
        extra_kwargs = {"password":{"write_only": True}}

    def create(self, validated_data):
        username = validated_data['username']
        phone_number = validated_data['phone_number']
        email = validated_data['email']
        password = validated_data['password']

        user = User(phone_number = phone_number, username = username, email = email)
        user.set_password(password)
        user.save()
        return user

class ProfileCreateSerializer(serializers.ModelSerializer):
    class Meta:
        """Profile serializer """
        model = Profile
        fields = '__all__'

class AccountInLIneSerializer(serializers.ModelSerializer):
    class Meta:
        """ Account serializer """
        model = Account
        fields = ['id', 'user_uuid', 'username','phone_number','email', 'password']
        extra_kwargs = {"password":{"write_only": True}}


class UserLoginSerializer(serializers.ModelSerializer):
    username = CharField()
    token = CharField(allow_blank = True , read_only = True)

    class Meta:
        """ User LOgin serializer """
        model = User
        fields = ['username', 'password', 'token']

    def validate(self, data):
        user_obj = None
        username = data.get("username", None)
        password = data["password"]
        if not username:
            raise ValidationError("A username is required to login")
        user = User.objects.filter(Q(username = username)).distinct()

        if user.exists() and user.count() == 1:
            user_obj = user.first()
        else :
            raise ValidationError("Username is invalid")

        if user_obj:
            if not user_obj.check_password(password):
                raise ValidationError("Incorrect credentials")

        data["token"] = "Successful"

        return data


class CheckNumberSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['phone_number',]

class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        """Use detail serializer """
        model = Account
        fields = ['username', 'email', ]

class AllUsersSerializer(serializers.ModelSerializer):
    class Meta:
        """ Get all users serializer """
        model = Account
        fields = '__all__'
        
#Subscription serializers
class SubscriptionSerializer(serializers.ModelSerializer):
    """Serializer to map the Model instance into JSON format."""
    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = Subscriptions
        fields = ('id', 'name', 'price_plan', 'description', 'date_created', 'date_modified')
        read_only_fields = ('date_created', 'date_modified')

#Membership serializers
class MembershipCreateSerializer(serializers.ModelSerializer):
    """ Membership model serializer """
    class Meta:
        """ Meta class to map model fields to serializer fields """
        model = Membership
        fields = '__all__'
