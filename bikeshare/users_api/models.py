# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import uuid
from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import PermissionsMixin
from django.db.models.signals import post_save
from django.utils import timezone
from django.utils.http import urlquote
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.dispatch import receiver

# Create your models here.

class AccountManager(BaseUserManager):
    def create_user(self,password=None,**kwargs):
        account = self.model(
            username = kwargs.get('username'),
            phone_number=kwargs.get('phone_number'),
            email=kwargs.get('email'),
            user_uuid=uuid.uuid4(),
        )
        account.set_password(password)
        account.save()

        return account
    
    def create_superuser(self, password, **kwargs):
        account = self.create_user(password,
                                   username=kwargs.get('username'),
                                   phone_number=kwargs.get('phone_number'),
                                   email=kwargs.get('email'),
                                   user_uuid=uuid.uuid4(),
                                   )

        account.is_admin = True
        account.is_staff = True
        account.is_superuser = True
        account.save()

        return account



class Account(AbstractBaseUser, PermissionsMixin):
    phone_number = models.CharField(max_length = 15, unique=True)
    email = models.EmailField(('email'), max_length=254, unique=False)
    username = models.CharField(('username'), max_length=40, unique=True, null=True)
    title = models.CharField(('title'), max_length=40, unique=False, null=True,default='user')
    user_uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    is_staff = models.BooleanField(('staff status'), default=False,
                                   help_text=('Designates whether the user can log on to the site'))
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(('date joined'), auto_now_add=True)
    date_modified = models.DateTimeField(('date modified'), auto_now=True)


    objects = AccountManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['phone_number','email']

    class Meta:
        verbose_name = ('user')
        verbose_name_plural = ('users')


    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username
        
    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{}".format(self.username)

class Profile(models.Model):
    user = models.OneToOneField(Account, on_delete=models.CASCADE,related_name = "account")
    id_number = models.CharField(max_length=15, blank=False)
    location = models.CharField(max_length=30, blank=True)
    gender = models.CharField(('gender'), blank=True, max_length=10)
    date_of_birth = models.DateField(blank=True)
    date_created = models.DateTimeField(auto_now=False, auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

#Subscription models
class Subscriptions(models.Model):
    """Subscription model """
    name = models.CharField(max_length=25, blank=False)
    price_plan = models.CharField(max_length=30)
    description = models.CharField(max_length=255, blank=True)
    date_created = models.DateTimeField(auto_now=False, auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{}".format(self.name)

#Membership model
class Membership(models.Model):
    """Membership model"""
    user = models.OneToOneField(Account, on_delete=models.CASCADE, related_name="membership_account")
    subscription = models.ManyToManyField(Subscriptions, related_name="membership_subscriptions")
    name = models.CharField(max_length=25, blank=False)
    date_created = models.DateTimeField(auto_now=False, auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{}".format(self.name)

