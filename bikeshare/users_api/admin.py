# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from users_api.models import Profile,Account

# Register your models here.
admin.site.register(Account)
admin.site.register(Profile)