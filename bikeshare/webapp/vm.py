from bikeshare_admin.models import *
from users_api.models import Account, Profile, Subscriptions, Membership
from django.db.models import Avg, Count


def get_bike_docks():
    '''return list of all bikedocks'''
    bikedocks = BikeDock.objects.all().order_by('name')
    count = BikeDock.objects.count()

    result = {
        'bikedocks': bikedocks,
        'count': count,
    }
    return result


def get_bike_users():
    '''return list of all bikedocks'''
    users = Account.objects.all().filter(is_staff=False).order_by('username')
    bikedocks = BikeDock.objects.all()
    count = Account.objects.count()

    result = {
        'users': users,
        'bikedocks': bikedocks,
        'count': count,
    }
    return result


def get_staff_users():
    '''return list of all bikedocks'''
    users = Account.objects.all().filter(is_staff=True).order_by('username')
    count = Account.objects.count()

    result = {
        'users': users,
        'count': count,
    }
    return result


def get_inride_users():
    '''return list of all bikedocks'''
    bikerequests = BikeRequest.objects.all(status="started")
    for bikerequest in bikerequests:
        users = Account.objects.all().filter(is_staff=False).order_by('username')
        count = Account.objects.count()

        result = {
            'users': users,
            'count': count,
        }
    return result


def get_user_profile(user_id):
    """return user profile"""
    user = Account.objects.all().filter(id=user_id)
    profile = Profile.objects.all().filter(user=user)
    rides = BikeRequest.objects.all().filter(user=user).count()
    count = 1

    result = {
        'user': user,
        'profile': profile,
        'rides': rides,
        'count': count,
    }
    return result


def get_bike_subscriptions():
    '''return list of all bikedocks'''
    subscriptions = Subscriptions.objects.all().order_by('id')
    count = Subscriptions.objects.count()

    result = {
        'subscriptions': subscriptions,
        'count': count,
    }
    return result


def get_dock_allocations():
    '''return list of all bikedocks'''
    allocations = DockAllocation.objects.all().order_by('id')
    count = DockAllocation.objects.count()

    result = {
        'allocations': allocations,
        'count': count,
    }
    return result


def get_bike_types():
    '''return list of all bikedocks'''
    biketypes = BikeType.objects.all().order_by('name')
    count = BikeDock.objects.count()

    result = {
        'biketypes': biketypes,
        'count': count,
    }
    return result


def get_bikes():
    '''return list of all bikedocks'''
    bikes = Bike.objects.all().order_by('id')
    count = Bike.objects.count()

    result = {
        'bikes': bikes,
        'count': count,
    }
    return result


def get_campaigns():
    '''return list of all bikedocks'''
    campaigns = Campaigns.objects.all().order_by('end_date')
    count = Campaigns.objects.count()

    result = {
        'campaigns': campaigns,
        'count': count,
    }
    return result


def get_ratings():
    '''return list of all bikedocks'''
    ratings = Rating.objects.all().order_by('date_created')
    average = Rating.objects.all().aggregate(Avg('rating'))
    count = Rating.objects.count()
    no_reviews = Rating.objects.filter(review=None).count()

    result = {
        'ratings': ratings,
        'average': average['rating__avg'],
        'count': count,
        'reviews_count': count - no_reviews,
    }
    return result


def get_bike_requests():
    requests = BikeRequest.objects.all().order_by('request_time')
    requests_count = BikeRequest.objects.count()

    accepted_requests = BikeRequest.objects.all().filter(status='accepted').order_by('request_time')
    accepted_requests_count = BikeRequest.objects.filter(status='accepted').count()

    returned_requests = BikeRequest.objects.all().filter(status='returned').order_by('request_time')
    returned_requests_count = BikeRequest.objects.filter(status='returned').count()

    accepted_requests_count = accepted_requests_count +returned_requests_count

    result = {
        'all':requests,
        'all_count': requests_count,
        'accepted':accepted_requests,
        'accepted_count':accepted_requests_count,
        'returned':returned_requests,
        'returned_count': returned_requests_count
    }

    return result
