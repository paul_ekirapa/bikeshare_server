# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from users_api.models import *
from bikeshare_admin.models import *
from django.http import HttpResponse, HttpResponseRedirect
from django.views import View
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from django.db.models import Sum
from .vm import *
import arrow

User = get_user_model()

from django.shortcuts import render


# Create your views here.
class IndexView(View):
    def get(self, request):
        context_dict = {}
        return render(request, 'webapp/entities/index.html', context_dict)


class AllDocksView(View):
    def get(self, request):
        dt = get_bike_docks()
        context_dict = {
            'bikedocks': dt['bikedocks'],
            'title': 'BikeDocks',
        }
        return render(request, 'webapp/entities/all_docks.html', context_dict)


class AddBikeDockView(View):
    def get(self, request):
        context_dict = {}
        return render(request, 'webapp/entities/add_dock.html', context_dict)

    def post(self, request):
        template = 'webapp/entities/add_dock.html'

        new_dock = BikeDock()
        new_dock.name = request.POST['name']
        new_dock.location = request.POST['location']
        new_dock.lat = request.POST['lat']
        new_dock.lng = request.POST['lng']
        new_dock.save()

        return HttpResponseRedirect(reverse("web-app:all-docks", ))


class AllStaffView(View):
    def get(self, request):
        dt = get_staff_users()

        context_dict = {
            'users': dt['users'],
            'title': 'Staff Users',
        }
        return render(request, 'webapp/entities/all_staff.html', context_dict)


class AddStaffView(View):
    def get(self, request):
        context_dict = {}
        return render(request, 'webapp/entities/add_staff.html', context_dict)

    def post(self, request):
        template = 'webapp/entities/add_staff.html'

        username = request.POST['username']
        phone_number = request.POST['phone_number']
        email = request.POST['email']
        title = request.POST['title']
        password = request.POST['password']

        User.objects.create_superuser(username=username, phone_number=phone_number, email=email, title=title,
                                      password=password)

        return HttpResponseRedirect(reverse("web-app:all-staff", ))


class AddUserView(View):
    def get(self, request):
        context_dict = {}
        return render(request, 'webapp/entities/add_user.html', context_dict)

    def post(self, request):
        template = 'webapp/entities/add_user.html'

        username = request.POST['username']
        phone_number = request.POST['phone_number']
        email = request.POST['email']
        password = request.POST['password']

        User.objects.create_user(username=username, phone_number=phone_number, email=email, title=title,
                                 password=password)

        return HttpResponseRedirect(reverse("web-app:all-users", ))


class AllUsersView(View):
    def get(self, request):
        dt = get_bike_users()

        context_dict = {
            'users': dt['users'],
            'title': 'Users',
        }
        return render(request, 'webapp/entities/all_users.html', context_dict)


class SubscriptionView(View):
    def get(self, request):
        dt = get_bike_subscriptions()
        context_dict = {
            'subscriptions': dt['subscriptions']
        }
        return render(request, 'webapp/entities/subscriptions.html', context_dict)

    def post(self, request):
        template = 'webapp/entities/subscriptions.html'

        new_sub = Subscriptions()
        new_sub.name = request.POST['name']
        new_sub.price_plan = request.POST['price_plan']
        new_sub.description = request.POST['description']
        new_sub.save()
        dt = get_bike_subscriptions()
        context_dict = {
            'subscriptions': dt['subscriptions']
        }
        return HttpResponseRedirect(reverse("web-app:subscriptions",))

class DockAllocationsView(View):
    def get(self, request):
        dt = get_dock_allocations()
        staff = get_staff_users()
        bikedocks = get_bike_docks()
        context_dict = {
            'allocations': dt['allocations'],
            'users': staff['users'],
            'bikedocks':bikedocks['bikedocks'],
        }
        return render(request, 'webapp/entities/dock_allocations.html', context_dict)

    def post(self, request):
        template = 'webapp/entities/dock_allocations.html'

        allocation = DockAllocation()
        user = request.POST['user']
        bike_dock = request.POST['bike_dock']
        allocated_by = request.POST['allocated_by']

        account = Account.objects.get(username=user)
        dock = BikeDock.objects.get(name=bike_dock)
        by = Account.objects.get(username=allocated_by)

        allocation.bike_dock = dock
        allocation.allocated_by = by
        allocation.user = account

        allocation.save()

        return HttpResponseRedirect(reverse("web-app:allocations",))

class AddBikeView(View):
    def get(self,request):
        dt =get_bike_types()
        context_dict = {
            'biketypes': dt['biketypes']
        }

        return render(request, 'webapp/entities/add_bike.html', context_dict)

    def post(self, request):
        bike = Bike()
        bike.bike_number = request.POST['bike_number']
        bike.bike_status = request.POST['bike_status']
        type = request.POST['bike_type']

        bike.bike_type = BikeType.objects.get(id=type)
        bike.save()

        return HttpResponseRedirect(reverse("web-app:all-bikes", ))

class AllBikesView(View):
    def get(self, request):
        dt = get_bikes()

        context_dict = {
            'bikes': dt['bikes'],
            'title': 'Bikes',
        }
        return render(request, 'webapp/entities/all_bikes.html', context_dict)


class CampaignsView(View):
    def get(self, request):
        dt = get_campaigns()

        context_dict = {
            'campaigns': dt['campaigns'],
            'title': 'Campaigns',
        }
        return render(request, 'webapp/entities/campaigns.html', context_dict)

class RatingsView(View):
    def get(self, request):
        dt = get_ratings()

        context_dict = {
            'ratings': dt['ratings'],
            'average':dt['average'],
            'count':dt['count'],
            'reviews_count':dt['reviews_count'],
            'title': 'Campaigns',
            'thirty_day_ratings':self.thirty_day_ratings(),
            'now':arrow.now(),
        }
        return render(request, 'webapp/entities/ratings.html', context_dict)

    def thirty_day_ratings(self):
        final_data = []

        date = arrow.now()
        for day in xrange(1, 30):
            date = date.replace(days=-1)
            count = Rating.objects.filter(
                date_created__gte=date.floor('day').datetime,
                date_created__lte=date.ceil('day').datetime).aggregate(Sum('rating'))
            result = count['rating__sum']
            if result==None:
                result = 0
            final_data.append(result)

        return final_data

class RequestsView(View):
    def get(self, request):
        dt = get_bike_requests()

        context_dict = {
            'all': dt['all'],
            'all_count': dt['all_count'],
            'accepted': dt['accepted'],
            'accepted_count': dt['accepted_count'],
            'returned': dt['returned'],
            'returned_count': dt['returned_count'],
            'title': 'Campaigns',
            'chart_data': self.chart_data(),
        }
        return render(request, 'webapp/entities/rides.html', context_dict)
    def chart_data(self):
        final_data = [0,1,3,1,1,0,4,5,0,1,10,12,9]
        for hour in xrange(1,12):
            by_hour =BikeRequest.objects.filter(request_received__hour = hour).count()
            #final_data.append(by_hour)

        return final_data
