from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from .views import IndexView, AllDocksView, AddBikeDockView, AllStaffView, AddStaffView, AllUsersView, AddUserView, \
    SubscriptionView, DockAllocationsView, AddBikeView, AllBikesView, CampaignsView, RatingsView, RequestsView

urlpatterns = [
    url(r'^index/', IndexView.as_view(), name='dashboard'),
    url(r'^bike-docks/', AllDocksView.as_view(), name='all-docks'),
    url(r'^add-dock/', AddBikeDockView.as_view(), name='add-dock'),
    url(r'^staff/', AllStaffView.as_view(), name='all-staff'),
    url(r'^add-staff/', AddStaffView.as_view(), name='add-staff'),
    url(r'^users/', AllUsersView.as_view(), name='all-users'),
    url(r'^add-user/', AddUserView.as_view(), name='add-user'),
    url(r'^subscriptions/', SubscriptionView.as_view(), name='subscriptions'),
    url(r'^allocations/', DockAllocationsView.as_view(), name='allocations'),
    url(r'^add-bike/', AddBikeView.as_view(), name='add-bike'),
    url(r'^bikes/', AllBikesView.as_view(), name='all-bikes'),
    url(r'^campaigns/', CampaignsView.as_view(), name='campaigns'),
    url(r'^ratings/', RatingsView.as_view(), name='ratings'),
    url(r'^rides/', RequestsView.as_view(), name='rides'),

]
