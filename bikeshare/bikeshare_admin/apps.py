# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
import receivers


class BikeshareAdminConfig(AppConfig):
    name = 'bikeshare_admin'

    def ready(self):
        from . import receivers