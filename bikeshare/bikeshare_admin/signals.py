from django.conf import settings
import django.dispatch

trial = django.dispatch.Signal(providing_args=["bikerequest",])
sendNotification = django.dispatch.Signal(providing_args=["bike_dock","sender_user"])
acceptRequestNotification = django.dispatch.Signal(providing_args=["bike_request","recepient_id"])
startRideNotification = django.dispatch.Signal(providing_args=["bike_request","recepient_id"])
cancelRequestNotification = django.dispatch.Signal(providing_args=["bike_request","recepient_id"])
returnBikeNotification = django.dispatch.Signal(providing_args=["bike_request","recepient_id"])
campaignNotification = django.dispatch.Signal(providing_args=["campaign"])

