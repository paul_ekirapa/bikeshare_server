# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import uuid
from django.db import models
from users_api.models import Account


# Create your models here.

# Bikes  type model
class BikeType(models.Model):
    """ Bikes model type model """
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=255)
    date_created = models.DateTimeField(auto_now=False, auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    objects = models.Manager()

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{}".format(self.name)


# Bike docks model
class BikeDock(models.Model):
    """ Bikedock models """
    name = models.CharField(max_length=30)
    location = models.CharField(max_length=25)
    lat = models.DecimalField(('Latitude'), max_digits=10, decimal_places=8)
    lng = models.DecimalField(('Longitude'), max_digits=11, decimal_places=8)
    date_created = models.DateTimeField(auto_now=False, auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{}".format(self.name)


# Bike model
class Bike(models.Model):
    """ Bike model class """
    bike_uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    bike_number = models.CharField(max_length=25)
    bike_status = models.CharField(max_length=25)
    is_bike_damaged = models.BooleanField(default=False)
    bike_type = models.ForeignKey(BikeType, on_delete=models.CASCADE, related_name='biketype')
    date_created = models.DateTimeField(auto_now=False, auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{}".format(self.bike_number)


class DockBikes(models.Model):
    bike_dock = models.ForeignKey(BikeDock, related_name='dock_bikes')
    number = models.IntegerField(default=0)


# Bike allocation to bike docks model
class BikeAllocation(models.Model):
    """ Bike allocation model """
    allocation_id = models.AutoField(primary_key=True)
    bike_dock = models.ForeignKey(BikeDock, related_name='dock_allocation')
    bike = models.ForeignKey(Bike, on_delete=models.CASCADE, related_name='bike_allocation')

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{}".format(self.allocation_id)


# Bike requests model
class BikeRequest(models.Model):
    """ Bike requests model """
    request_uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    user = models.ForeignKey(Account, on_delete=None, related_name='user_request')
    bike = models.ForeignKey(Bike, related_name='bike', default=1)
    bike_dock = models.ForeignKey(BikeDock, related_name='origin_dock', default=1,on_delete=None)
    return_dock = models.IntegerField(blank=True, default=1)
    status = models.CharField(max_length=20)
    is_served = models.BooleanField(default=False)
    request_time = models.DateTimeField(blank=True)
    usage_reason = models.CharField(max_length=50,default="recreation", blank=True)
    request_received = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{} {}".format(self.user, self.bike_dock)


# Dock alocation to staff
class DockAllocation(models.Model):
    """Dock allocation model"""
    allocation_uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    user = models.ForeignKey(Account, on_delete=None, related_name='user_dock_allocation')
    bike_dock = models.ForeignKey(BikeDock, related_name='allocation_dock')
    allocated_by = models.ForeignKey(Account, on_delete=None, related_name='allocated_by', default=1)
    date_created = models.DateTimeField(auto_now=False, auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{} {}".format(self.user, self.bike_dock)


class Rating(models.Model):
    rating = models.FloatField(default=0)
    bike_request = models.ForeignKey(BikeRequest, on_delete=None, related_name='bike_request')
    review = models.CharField(max_length=502, blank=True,null=True,default=None)
    date_created = models.DateTimeField(auto_now=False, auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)


class Feedback(models.Model):
    feedback = models.CharField(max_length=502)
    user = models.ForeignKey(Account, on_delete=None, related_name='user_feedback')
    date_created = models.DateTimeField(auto_now=False, auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)


class RideStats(models.Model):
    distance = models.FloatField(default=0)
    speed = models.FloatField(default=0)
    calories = models.FloatField(default=0)
    duration = models.FloatField(default=0)
    score = models.FloatField(default=0)
    bike_request = models.ForeignKey(BikeRequest, on_delete=None, related_name='bike_request_stats')
    user = models.ForeignKey(Account, on_delete=None, related_name='user_ride_stats', default=1)
    date_created = models.DateTimeField(auto_now=False, auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)


class Campaigns(models.Model):
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=502)
    image = models.ImageField(blank=True, null=True, upload_to='Campaigns/',
                              default='Campaigns/None/No-Campaign.jpg')  # Set default to bikeshare logo
    end_date = models.DateTimeField(blank=True)


class Trail(models.Model):
    trial = models.IntegerField()
