# View model file. All db methods
from __future__ import print_function
from __future__ import print_function
from .models import *
from push_notifications.models import GCMDevice


def getDockBikesCount(bike_dock_id):
    bikes = BikeAllocation.objects.filter(bike_dock=bike_dock_id).count()


def getDockQueue(bike_dock_id):
    bikes = BikeRequest.objects.filter(bike_dock=bike_dock_id).count()
    results = {
        'in_waiting': bike_dock_id,
    }
    return results


def sendPushToDevice(bike_dock, sender_user):
    """Pass the user id to send push notification"""
    try:

        allocations = DockAllocation.objects.filter(bike_dock=bike_dock).get()
        if allocations:
            user = Account.objects.filter(id=allocations.user.id).get()
            # device = GCMDevice.objects.filter(user=user.id).get().latest()
            device = GCMDevice.objects.filter(user=user.id)

            message = {
                "notification": {
                    "title": "New Request",
                    "body": "New bike request from {}".format(sender_user.username),
                }
            }
            device.send_message(message="New Bike Request", extra={"title": "Bike Request", "data": "New Request"},
                                use_fcm_notifications=False)

    except DockAllocation.DoesNotExist:
        print("error")


def sendAcceptRequestNotification(bike_request, recepient_id):
    """ Pass the bike request and the recepient user id"""
    try:
        user = Account.objects.filter(id=recepient_id).get()
        if user:
            device = GCMDevice.objects.filter(user=user.id)
            device.send_message(message="Accepted", extra={"title": "Request Accepted", "data": "Accepted"},
                                use_fcm_notifications=False)

        else:
            trail = Trail.objects.create(trial=404)
            trail.save()
    except Account.DoesNotExist:
        print("error")


def sendStartRidetNotification(bike_request, recepient_id):
    """ Pass the bike request and the recepient user id"""
    try:
        user = Account.objects.filter(id=recepient_id).get()
        if user:
            device = GCMDevice.objects.filter(user=user.id)
            device.send_message(message="Started", extra={"title": "Ride Started", "data": "Started"},
                                use_fcm_notifications=False)

        else:
            trail = Trail.objects.create(trial=404)
            trail.save()
    except Account.DoesNotExist:
        print("error")


def sendCancelRequestNotification(bike_request, recepient_id):
    """ Pass the bike request and the recepient user id"""
    try:
        user = Account.objects.filter(id=recepient_id).get()
        if user:
            device = GCMDevice.objects.filter(user=user.id)
            device.send_message(message="Cancelled", extra={"title": "Request Cancelled", "data": "Cancelled"},
                                use_fcm_notifications=False)

        else:
            trail = Trail.objects.create(trial=404)
            trail.save()
    except Account.DoesNotExist:
        print("error")


def sendReturnBikeNotification(bike_request, recepient_id):
    """ Pass the bike request and the recepient user id"""
    try:
        user = Account.objects.filter(id=recepient_id).get()
        if user:
            device = GCMDevice.objects.filter(user=user.id)
            device.send_message(message="Returned", extra={"title": "Bike Returned", "data": "Returned"},
                                use_fcm_notifications=False)

        else:
            trail = Trail.objects.create(trial=404)
            trail.save()
    except Account.DoesNotExist:
        print("error")


def sendCampaigns(campaign):
    try:
        users = Account.objects.filter(is_staff=False)
        if users:
            if len(users) > 1:
                for user in users:
                    try:
                        device = GCMDevice.objects.filter(user=user.id)
                        device.send_message(message="Campaign",
                                            extra={"title": campaign.title,
                                                   "data": "Campaign",
                                                   "id": campaign.id,
                                                   "description": campaign.description},
                                            use_fcm_notifications=False)
                    except Account.DoesNotExist:
                        trail = Trail.objects.create(trial=111)
                        trail.save()
            else:
                users = users.get()
                device = GCMDevice.objects.filter(user=users.id)
                device.send_message(message="Campaign",
                                    extra={"title": campaign.title,
                                           "data": "Campaign",
                                           "id": campaign.id,
                                           "description": campaign.description},
                                    use_fcm_notifications=False)
                trail = Trail.objects.create(trial=999)
                trail.save()

    except Account.DoesNotExist:
        print('error')


def sendPushNotification():
    # device = GCMDevice.objects.all(registration_id="cXR7dRXYokY:APA91bFjuLw1BhRiBRd0qjoyl4fq0SXQLassfeLlh6HhZxJvELIPsk2r4tljWnR3aFl8xj8V-6_OEf_RgBsbF4Vm1ASawEIYoy7myWGB206bpQkfw2HEbzQpGWiEeREhyT9zNbVqfJMY")
    user = Account.objects.filter(username="ekirapa").get()
    if user:
        fcm_device = GCMDevice.objects.create(
            registration_id="cXR7dRXYokY:APA91bFjuLw1BhRiBRd0qjoyl4fq0SXQLassfeLlh6HhZxJvELIPsk2r4tljWnR3aFl8xj8V-6_OEf_RgBsbF4Vm1ASawEIYoy7myWGB206bpQkfw2HEbzQpGWiEeREhyT9zNbVqfJMY",
            cloud_message_type="FCM", user=user)
        if fcm_device:
            fcm_device.send_message("You've got mail")
