# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView, ListCreateAPIView, ListAPIView, RetrieveAPIView, \
    RetrieveUpdateDestroyAPIView
from rest_framework.views import APIView
from rest_framework import generics
from rest_framework import viewsets
from rest_framework.mixins import RetrieveModelMixin, UpdateModelMixin, CreateModelMixin
from rest_framework.decorators import detail_route,parser_classes
from rest_framework.parsers import FormParser, MultiPartParser
from .models import BikeType, BikeDock, Bike, BikeAllocation, BikeRequest, DockBikes, DockAllocation, Rating, Feedback, RideStats, Campaigns
from .serializers import CreateBikeTypeSerializer, BikeDockSerializer, CreateBikeSerializer, \
    CreateBikeAllocationSerializer, BikeRequestSerializer, DockBikesSerializer, FCMDeviceSerializer, \
    DockRequestSerializer, DockAllocationSerializer, BikeUpdateRequestSerializer, RatingSerializer, FeedbackSerializer, RideStatsSerializer, CampaignSerializer
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_201_CREATED
from push_notifications.models import GCMDevice
from .view_model import *


# Create your views here.

class BikeTypeView(ListCreateAPIView):
    """ Creat/View  Bike type """
    permission_classes = [AllowAny, ]
    serializer_class = CreateBikeTypeSerializer
    queryset = BikeType.objects.all()


class BikeDockView(ListCreateAPIView):
    """ Creat/View  Bike dock """
    permission_classes = [AllowAny, ]
    serializer_class = BikeDockSerializer
    queryset = BikeDock.objects.all()


class BikeView(ListCreateAPIView):
    """ Create/View bikes """
    permission_classes = [AllowAny, ]
    serializer_class = CreateBikeSerializer
    queryset = Bike.objects.all()


class BikeAllocationView(ListCreateAPIView):
    """ Create/View bike allocations"""
    permission_classes = [AllowAny, ]
    serializer_class = CreateBikeAllocationSerializer
    queryset = BikeAllocation.objects.all()


class BikeRequestView(ListCreateAPIView):
    """ Create/view bike requests """
    permission_classes = [AllowAny, ]
    serializer_class = BikeRequestSerializer
    queryset = BikeRequest.objects.all()


class InRideBikeRequestView(ListCreateAPIView):
    """ Create/view bike requests """
    permission_classes = [AllowAny, ]
    serializer_class = DockRequestSerializer

    # queryset = BikeRequest.objects.filter(status="started")

    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        return BikeRequest.objects.filter(status="started")


class DockBikesView(ListCreateAPIView):
    permission_classes = [AllowAny, ]
    serializer_class = DockBikesSerializer
    queryset = DockBikes.objects.all()


class DockQueueView(ListAPIView):
    permission_classes = [AllowAny, ]
    serializer_class = BikeRequestSerializer
    queryset = BikeRequest.objects.all()

    def get_queryset(self):
        dock_id = self.kwargs['dock_id']
        return BikeRequest.objects.filter(bike_dock=dock_id, is_served=False)

    def get(self, request, dock_id):
        results = {
            'in_waiting': self.get_queryset().count(),
        }
        return Response(results, status=HTTP_200_OK)


class FCMDeviceView(ListCreateAPIView):
    permission_classes = [AllowAny, ]
    serializer_class = FCMDeviceSerializer
    queryset = GCMDevice.objects.all()


class SendFCMNotification(APIView):
    permission_classes = [AllowAny, ]

    def get(self, request):
        sendPushNotification()


class DockBikeRequests(ListAPIView):
    permission_classes = [AllowAny, ]
    serializer_class = DockRequestSerializer
    # queryset = BikeRequest.objects.filter(is_served=True)
    lookup_field = 'bike_dock'

    def get_queryset(self):
        bike_dock = self.kwargs['bike_dock']
        return BikeRequest.objects.filter(bike_dock=bike_dock, is_served=False)


class AcceptBikeRequest(RetrieveUpdateDestroyAPIView):
    permission_classes = [AllowAny, ]
    serializer_class = BikeUpdateRequestSerializer
    lookup_field = 'id'
    queryset = BikeRequest.objects.filter()


class StartRide(RetrieveUpdateDestroyAPIView):
    permission_classes = [AllowAny, ]
    serializer_class = BikeUpdateRequestSerializer
    lookup_field = 'id'
    queryset = BikeRequest.objects.filter(is_served=True)


class DockAllocationView(ListAPIView):
    permission_classes = [AllowAny, ]
    serializer_class = DockAllocationSerializer
    lookup_field = 'user'

    def get_queryset(self):
        user = self.kwargs['user']
        return DockAllocation.objects.filter(user=user)

class UserRequestsView(ListAPIView):
    permission_classes = [AllowAny, ]
    serializer_class = DockRequestSerializer
    lookup_field = 'user'

    def get_queryset(self):
        user = self.kwargs['user']
        return BikeRequest.objects.filter(user=user,status='returned')


class RatingView(ListCreateAPIView):
    """ Create/view bike requests """
    permission_classes = [AllowAny, ]
    serializer_class = RatingSerializer
    queryset = Rating.objects.all()

class FeedbackView(ListCreateAPIView):
    """ Create/view bike requests """
    permission_classes = [AllowAny, ]
    serializer_class = FeedbackSerializer
    queryset = Feedback.objects.all()

class RidestatsView(ListCreateAPIView):
    """ Create/view bike requests """
    permission_classes = [AllowAny, ]
    serializer_class = RideStatsSerializer
    queryset = RideStats.objects.all()

class RequestRideStatsView(RetrieveUpdateDestroyAPIView):
    permission_classes = [AllowAny, ]
    serializer_class = RideStatsSerializer
    lookup_field = 'bike_request'

    def get_queryset(self):
        bike_request = self.kwargs['bike_request']
        return RideStats.objects.filter(bike_request=bike_request)

class CampaignsView(ListCreateAPIView):
    permission_classes = [AllowAny, ]
    serializer_class = CampaignSerializer
    queryset = Campaigns.objects.all()