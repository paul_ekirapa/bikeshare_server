from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from .views import BikeTypeView, BikeDockView, BikeView, BikeAllocationView, BikeRequestView, DockBikesView, \
    DockQueueView, FCMDeviceView, SendFCMNotification, DockBikeRequests, DockAllocationView, AcceptBikeRequest, \
    InRideBikeRequestView, RatingView, FeedbackView, UserRequestsView, RidestatsView, RequestRideStatsView,CampaignsView

urlpatterns = [
    url(r'^bike-type/', BikeTypeView.as_view(), name='create-biketype'),
    url(r'^bike-dock/', BikeDockView.as_view(), name='create-bike-dock'),
    url(r'^bike/', BikeView.as_view(), name='create-bike'),
    url(r'^bike-allocation/', BikeAllocationView.as_view(), name='bike-allocation'),
    url(r'^bike-request/', BikeRequestView.as_view(), name='bike-request'),
    url(r'^in-ride', InRideBikeRequestView.as_view(), name='in-ride'),
    url(r'^bike-requests/(?P<bike_dock>\d+)/$', DockBikeRequests.as_view(), name='dock-requests'),
    url(r'^dock-requests/(?P<id>\d+)/$', AcceptBikeRequest.as_view(), name='accept-dock-requests'),
    url(r'^start-ride/(?P<id>\d+)/$', AcceptBikeRequest.as_view(), name='startRide'),
    url(r'^dock-bikes/', DockBikesView.as_view(), name='dock-bikes'),
    url(r'^dock-queue/(?P<dock_id>[0-9]+)/$', DockQueueView.as_view(), name='dock-queue'),
    url(r'^fcm-device/', FCMDeviceView.as_view(), name='fcm-device'),
    url(r'^notif/', SendFCMNotification.as_view(), name='fcm-notif'),
    url(r'^dock-allocation/(?P<user>\d+)/$', DockAllocationView.as_view(), name='dock-allocation'),
    url(r'^user-request/(?P<user>\d+)/$', UserRequestsView.as_view(), name='user-requests'),
    url(r'^rating/', RatingView.as_view(), name='ratings'),
    url(r'^feedback/', FeedbackView.as_view(), name='feedback'),
    url(r'^ridestats/', RidestatsView.as_view(), name='ridestats'),
    url(r'^ridestat/(?P<bike_request>\d+)/$', RequestRideStatsView.as_view(), name='user-requests'),
    url(r'^campaigns/', CampaignsView.as_view(), name='campaigns'),

]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
