from django.dispatch import receiver
from .models import Trail
from .view_model import *
from .signals import *


@receiver(trial)
def save_trial(sender, **kwargs):
    print ("working well")
    trail = Trail.objects.create(trial=1)
    trail.save()


@receiver(sendNotification)
def on_new_request(sender, **kwargs):
    bike_dock = kwargs.get('bike_dock')
    sender_user = kwargs.get('sender_user')
    sendPushToDevice(bike_dock=bike_dock, sender_user=sender_user)


@receiver(acceptRequestNotification)
def on_accept_request(sender, **kwargs):
    bike_request = kwargs.get('bike_request')
    recepient_id = kwargs.get('recepient_id')
    sendAcceptRequestNotification(bike_request=bike_request, recepient_id=recepient_id)


@receiver(startRideNotification)
def on_start_request(sender, **kwargs):
    bike_request = kwargs.get('bike_request')
    recepient_id = kwargs.get('recepient_id')
    sendStartRidetNotification(bike_request=bike_request, recepient_id=recepient_id)


@receiver(cancelRequestNotification)
def on_cancel_request(sender, **kwargs):
    bike_request = kwargs.get('bike_request')
    recepient_id = kwargs.get('recepient_id')
    sendCancelRequestNotification(bike_request=bike_request, recepient_id=recepient_id)


@receiver(returnBikeNotification)
def on_bike_returned(sender, **kwargs):
    bike_request = kwargs.get('bike_request')
    recepient_id = kwargs.get('recepient_id')
    sendReturnBikeNotification(bike_request=bike_request, recepient_id=recepient_id)


@receiver(campaignNotification)
def on_new_campaign(sender, **kwargs):
    campaign = kwargs.get('campaign')
    sendCampaigns(campaign)
