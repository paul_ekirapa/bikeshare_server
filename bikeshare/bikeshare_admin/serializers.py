from rest_framework import serializers
from rest_framework.serializers import ValidationError, CharField, EmailField
from .models import BikeType, BikeDock, Bike, BikeAllocation, BikeRequest, DockBikes, DockAllocation, Trail, Rating, \
    Feedback, RideStats, Campaigns
from users_api.models import Account
from users_api.serializers import AccountInLIneSerializer
from push_notifications.models import GCMDevice
from .receivers import trial, sendNotification, acceptRequestNotification, startRideNotification, \
    cancelRequestNotification, returnBikeNotification, campaignNotification


# Bike type serializers
class CreateBikeTypeSerializer(serializers.ModelSerializer):
    """ Bike type model serializer """

    class Meta:
        """ Meta class to map biketype model fields to serializer fields """
        model = BikeType
        fields = '__all__'


# Bike Dock serializers
class BikeDockSerializer(serializers.ModelSerializer):
    """ Bikedock model serializer """

    class Meta:
        model = BikeDock
        fields = '__all__'


# Bikes  serializers
class CreateBikeSerializer(serializers.ModelSerializer):
    """ Bikedock model serializer """

    class Meta:
        model = Bike
        fields = '__all__'


# Bike Allocation to docks serializers
class CreateBikeAllocationSerializer(serializers.ModelSerializer):
    """ Biked allocation model serializer """

    class Meta:
        model = BikeAllocation
        fields = '__all__'


# Bike request serializers
class BikeRequestSerializer(serializers.ModelSerializer):
    """ Create bike request serializer """

    class Meta:
        """ Meta class for bike request create serializer """
        model = BikeRequest
        fields = '__all__'

    def create(self, validated_data):
        bikerequest = BikeRequest.objects.create(**validated_data)
        bike_dock = validated_data['bike_dock']
        user = validated_data['user']
        bikerequest.save()

        # vm.sendPushToDevice(bike_dock, user, "New bike request")
        # trial.send(sender=BikeRequest, bikerequest=bikerequest)
        sendNotification.send(sender=BikeRequest, bike_dock=bike_dock, sender_user=user)
        return bikerequest


class BikeUpdateRequestSerializer(serializers.ModelSerializer):
    class Meta:
        """ Meta class for bike request create serializer """
        model = BikeRequest
        fields = '__all__'

    def update(self, instance, validated_data):
        bike_request = BikeRequest(**validated_data)
        # instance = bike_request
        data = validated_data
        user_data = data.pop('user')

        instance.is_served = validated_data['is_served']
        instance.status = validated_data['status']
        instance.return_dock = validated_data['return_dock']
        status = bike_request.status
        instance.save()

        if validated_data['status'] == "accepted":
            acceptRequestNotification.send(sender=BikeRequest, bike_request=bike_request, recepient_id=user_data.id)
            trail = Trail.objects.create(trial=101)
            trail.save()
        elif validated_data['status'] == "started":
            trail = Trail.objects.create(trial=201)
            trail.save()
            startRideNotification.send(sender=BikeRequest, bike_request=bike_request, recepient_id=user_data.id)
        elif validated_data['status'] == "cancelled":
            trail = Trail.objects.create(trial=301)
            trail.save()
            cancelRequestNotification.send(sender=BikeRequest, bike_request=bike_request, recepient_id=user_data.id)
        elif validated_data['status'] == "returned":
            returnBikeNotification.send(sender=BikeRequest, bike_request=bike_request, recepient_id=user_data.id)
            trail = Trail.objects.create(trial=401)
            trail.save()

        return instance


class DockBikesSerializer(serializers.ModelSerializer):
    """ Create dock bikes serializer """

    class Meta:
        """ Meta class for bike request create serializer """
        model = DockBikes
        fields = '__all__'


class DockRequestSerializer(serializers.ModelSerializer):
    """Returns requests from a specific bike dock"""
    user = AccountInLIneSerializer()

    class Meta:
        """ Meta class for bike request create serializer """
        model = BikeRequest
        fields = (
            'id', 'request_uuid', 'return_dock', 'status', 'is_served','usage_reason', 'request_time', 'request_received', 'user',
            'bike_dock',)


class FCMDeviceSerializer(serializers.ModelSerializer):
    """ Create dock bikes serializer """

    class Meta:
        """ Meta class for bike request create serializer """
        model = GCMDevice
        fields = '__all__'


class DockAllocationSerializer(serializers.ModelSerializer):
    class Meta:
        """ Meta class for dock allocation serializer """
        model = DockAllocation
        fields = '__all__'


class RatingSerializer(serializers.ModelSerializer):
    """Serializer class for ratings"""

    class Meta:
        model = Rating
        fields = ('rating','bike_request','review','date_created')


class FeedbackSerializer(serializers.ModelSerializer):
    """Serializer class for ratings"""

    class Meta:
        model = Feedback
        fields = '__all__'


class RideStatsSerializer(serializers.ModelSerializer):
    """ Biked allocation model serializer """

    class Meta:
        model = RideStats
        fields = '__all__'


class CampaignSerializer(serializers.HyperlinkedModelSerializer):
    image = serializers.ImageField(max_length=None,use_url=True, allow_empty_file=True)

    class Meta:
        model = Campaigns
        fields = ( 'title', 'description', 'image','end_date')
        #read_only_fields = ('url', 'image')

    def create(self, validated_data):
        campaign = Campaigns.objects.create(**validated_data)
        campaign.save()

        # vm.sendPushToDevice(bike_dock, user, "New bike request")
        # trial.send(sender=BikeRequest, bikerequest=bikerequest)
        campaignNotification.send(sender=Campaigns, campaign=campaign)
        return campaign

