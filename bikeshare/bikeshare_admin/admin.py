# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.utils.translation import ugettext_lazy
from .models import BikeRequest, DockAllocation, Trail, Feedback, Rating, RideStats, Campaigns, BikeDock, DockBikes,Bike,BikeType
from push_notifications.models import GCMDevice

# Register your models here.
#admin.site.register(BikeDock)

class MyAdminSite(admin.AdminSite):
    site_title = ugettext_lazy('Bike share management')
    site_header = ugettext_lazy('Management')
    index_title = ugettext_lazy('Bikeshare Management')

admin_site=MyAdminSite()
admin_site.register(BikeDock)
admin_site.register(DockAllocation)
admin_site.register(Trail)
admin_site.register(Rating)
admin_site.register(RideStats)
admin_site.register(Campaigns)
admin_site.register(BikeRequest)
admin_site.register(Bike)
admin_site.register(BikeType)



admin_site.register(DockBikes)

