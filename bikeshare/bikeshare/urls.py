"""bikeshare URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from  bikeshare_admin.admin import admin_site

urlpatterns = [
    url(r'^admin/', include(admin_site.urls)),
    url(r'^api/', include("users_api.urls", namespace='users-api')),
    url(r'^site/', include("webapp.urls", namespace='web-app')),
    url(r'^api/manage/', include("bikeshare_admin.urls", namespace='management-api')),
    url(r'^docs/', include('rest_framework_docs.urls')),

]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
